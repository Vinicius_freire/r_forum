Rails.application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  authenticated :user do
    root to: "posts#index", as: :authenticated_root
    resources :posts, only: [:index, :show, :create, :destroy]
    resources :comments, only: [:show, :create]
  end

  unauthenticated :user do
    root to: "home#welcome"
  end
end
