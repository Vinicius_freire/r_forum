class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
    skip_before_action :verify_authenticity_token
    include Pundit

    private

    def user_not_authorized
        redirect_to request.referrer, flash: {alert: "Unauthorized action"}
    end

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :nickname])
        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :nickname])
    end
end
