class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: [:show, :destroy]
  before_action :authorize_post, only: [:destroy]

  def index
    @posts = Post.all.order(created_at: :desc).page(params[:page])
    @post = Post.new
  end

  def show
  end

  def create
    filtered_post = BlacklistService.new(post_params[:title]).perform
    @post = Post.new({title: filtered_post}.merge(user: current_user))
    if @post.save
      redirect_to @post, flash: {success: "Post was successfully created"}
    else
      redirect_to posts_path, flash: {error: "Something went wrong!"}
    end
  end

  def destroy
    if @post.delete
      redirect_to posts_url, flash: {success: "Post was successfully destroyed"}
    else
      redirect_to @post, flash: {error: "Something went wrong!"}
    end
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title)
  end

  def authorize_post
    authorize @post
  end

end
