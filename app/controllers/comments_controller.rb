class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_comment, only: [:show]
  before_action :set_commentable, only: [:create]

  def show
    render json: @comments
  end

  def create
    filtered_comment = BlacklistService.new(comment_params[:comment]).perform
    @comment = Comment.new({comment: filtered_comment}.merge(user: current_user, commentable: @commentable))
    if @comment.save
      redirect_to request.referrer, flash: {success: "Comment was successfully created"}
    else
      redirect_to posts_path, flash: {error: "Something went wrong!"}
    end
  end

  private
  
  def set_comment
    @comments = Post.find(params[:id]).comments.order(created_at: :desc)
  end

  def set_commentable
    @commentable = Comment.find(params[:comment_id]) if params[:comment_id]
    @commentable = Post.find(params[:post_id]) if params[:post_id]
  end

  def comment_params
    params.require(:comment).permit(:comment)
  end
end
