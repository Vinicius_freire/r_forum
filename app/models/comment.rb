class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  field :comment, type: String

  belongs_to :user
  belongs_to :commentable, polymorphic: true
  has_many :comments, as: :commentable, dependent: :destroy
end
