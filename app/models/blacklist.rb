class Blacklist
  include Mongoid::Document
  field :word, type: String

  validates :word, presence: true, uniqueness: true

  after_save :save_variations
  
  def save_variations
    words = create_variations self.word
    pluralized_words = create_variations self.word.pluralize
    (words+pluralized_words).each do |word|
      Blacklist.create(word: word)
    end
  end

  private

  def create_variations word
    words = []
    
    new_word = word.downcase.gsub(/a|e|i|o|u/) do |m|
      case m.downcase
      when 'a'
        '@'
      when 'e'
        '&'
      when 'i'
        "1"
      when 'o'
        "0"
      when 'u'
        "u"
      end
    end
  
    words << new_word unless words.include? new_word
  
    word.split("").each_with_index do |char,index|
      case char.downcase
      when 'a'
        start = word[0..index].gsub(/a/,"@")
        new_word = start + word[index+1..-1]
      when 'e'
        start = word[0..index].gsub(/e/,"&")
        new_word = start + word[index+1..-1]
      when 'i'
        start = word[0..index].gsub(/i/,"1")
        new_word = start + word[index+1..-1]
      when 'o'
        start = word[0..index].gsub(/o/,"0")
        new_word = start + word[index+1..-1]
      when 'u'
        start = word[0..index].gsub(/u/,"y")
        new_word = start + word[index+1..-1]
      end
      words << new_word unless words.include? new_word
    end
  
    words
  end

end
