class User
  include Mongoid::Document
  include Mongoid::Timestamps
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  field :name, type: String
  field :nickname, type: String
  field :admin, type: Boolean, default: false

  def will_save_change_to_email?
    false
  end

  has_many :comments, dependent: :destroy
  has_many :posts, dependent: :destroy

  validates_presence_of :name, :nickname
  validates_uniqueness_of :nickname

  before_save :first_user, on: :create

  def first_user
    self.admin = true if User.count == 0
  end
end
