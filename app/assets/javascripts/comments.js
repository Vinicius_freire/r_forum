$(document).on('ready turbolinks:load', function(){
  var post_id = -1
  var base_url = ""

  if(window.location.href.split("/")[3] == "posts"){
    let url = window.location.href.split("/")
    base_url = url[0]+"/"+url[1]+"/"+url[2]+"/"
    post_id = url[4];
    load_comments();
  }

  function load_comments(){
    
    $("#button_comment_post").click(function(){
      $('#comment_post').removeAttr('hidden');
    });

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": base_url+"comments/"+post_id,
      "method": "GET",
      "headers": {
          "cache-control": "no-cache"
      }
    }

    $.ajax(settings).done(function (response) {
      load_tree_coments(response)
    });    
  }


  function load_tree_coments(comments){
    $("#all_comments").html('').prepend("");

    $.each( comments, function( key, value ) {
     
      let comment_id = value.id["$oid"]
      let current_datetime = new Date(value.created_at)
      const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let formatted_date = current_datetime.getDate()+ " " + months[current_datetime.getMonth()]+ " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() 
      
      let middle = `<div class="card-action white-text grey darken-2">
                    <p><a> ${formatted_date} | ${value.user.nickname}: </a> ${value.comment}
                    (${value.comments.length} comments) 
                    <i class="material-icons right" id=${"button_"+comment_id}>comment</i></p>
                    <div class="card-action white-text grey darken-2" id= ${"comment_"+comment_id} hidden>
                      <form class="new_comment" id="new_comment" action="/comments" accept-charset="UTF-8" method="post">
                          <div class="input-field col s6">
                              <input class="validate white-text" required="required" type="text" name="comment[comment]" id="comment_comment" />
                              <label for="first_name">Comment</label>
                          </div>
                          <div class="input-field col s6">
                              <button class="btn waves-effect waves-light" type="submit" name="commit">Create
                                  <i class="material-icons right">send</i>
                              </button>
                          </div>
                          <div class="input-field col s6" hidden>
                              <input type="hidden" name="comment_id" id="comment_id" value=${comment_id}>
                          </div>
                      </form>
                  </div>
                </div>`;

      $("#all_comments").prepend(middle);

      $("#button_" + comment_id).click(function(){
        if($("#comment_" + comment_id).attr( 'hidden' ) != null ){
          $("#comment_" + comment_id).removeAttr('hidden');
        }else{
          $("#comment_" + comment_id).attr('hidden',true);
        }
      });

      if(value.comments.length>0){
        load_leaf_comments(value.comments, value.comment)
      }

    });

  }

  function load_leaf_comments(comments, post){
    $.each( comments, function( key, value ) {
      let comment_id = value[0]._id["$oid"]
      let current_datetime = new Date(value[0].created_at)
      const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let formatted_date = current_datetime.getDate()+ " " + months[current_datetime.getMonth()]+ " " + current_datetime.getHours() + ":" + current_datetime.getMinutes()
      let n_comments = value[2].length

      let son = `<div class="card-action white-text grey darken-2">
                    <p style="color: #9e9e9e;">Comment: ${post}</p>
                    <p><a> ${formatted_date} | ${value[1].nickname}: </a> ${value[0].comment}
                    (${n_comments} comments) 
                    <i class="material-icons right" id=${"button_"+comment_id}>comment</i></p>
                    <div class="card-action white-text grey darken-2" id= ${"comment_"+comment_id} hidden>
                      <form class="new_comment" id="new_comment" action="/comments" accept-charset="UTF-8" method="post">
                          <div class="input-field col s6">
                              <input class="validate white-text" required="required" type="text" name="comment[comment]" id="comment_comment" />
                              <label for="first_name">Comment</label>
                          </div>
                          <div class="input-field col s6">
                              <button class="btn waves-effect waves-light" type="submit" name="commit">Create
                                  <i class="material-icons right">send</i>
                              </button>
                          </div>
                          <div class="input-field col s6" hidden>
                              <input type="hidden" name="comment_id" id="comment_id" value=${comment_id}>
                          </div>
                      </form>
                  </div>
                </div>`;

      $("#all_comments").prepend(son);

      $("#button_" + comment_id).click(function(){
        if($("#comment_" + comment_id).attr( 'hidden' ) != null ){
          $("#comment_" + comment_id).removeAttr('hidden');
        }else{
          $("#comment_" + comment_id).attr('hidden',true);
        }
      });

      if(n_comments>0){
        load_leaf_comments(value[2],value[0].comment)
      }

    });
  }  

});