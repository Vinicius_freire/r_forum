class BlacklistService
    def initialize title
        @title = title
    end

    def perform
        filtered_pharse
    end

    private

    def filtered_pharse
        blacklist = Blacklist.all.map {|x| x.word.downcase}
        return @title if blacklist.count == 0
        regex = Regexp.new(blacklist.join("|"))
        @title.downcase.gsub(regex,"*****")
    end
end