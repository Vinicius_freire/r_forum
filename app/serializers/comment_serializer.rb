class CommentSerializer < ActiveModel::Serializer
  attributes :id, :comment, :commentable_id, :commentable_type, :comments, :user, :created_at

  def comments
    trees = []
    comments = Comment.all
    groups = comments.group_by{ |x| x.commentable_id }
    groups.default = []

    build_tree = lambda do |comment|
      [comment,comment.user, groups[comment[:id]].map(&build_tree)]
    end

    object.comments.each do |comment|
      trees << build_tree[comment]
    end
    trees
  end

end

