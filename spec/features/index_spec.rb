require "rails_helper"

RSpec.feature "User create posts and comments" do

    scenario "user visit the root_path and login" do

      visit root_path

      expect(page).to have_content("Welcome to DISCUSSION FORUM.")
      
      click_link("Sign Up", :match => :first)
      fill_in "user[name]", with: "Teste View"
      fill_in "user[nickname]", with: "feature"
      fill_in "user[email]", with: "teste@email.com"
      fill_in "user[password]", with: "secret123"
      fill_in "user[password_confirmation]", with: "secret123"
      click_button "commit"

      4.times do
        FactoryBot.create(:post)
      end

      visit  root_path

      Post.all.each do |post|
          expect(page).to have_content(post.user.nickname.capitalize)
          expect(page).to have_content(post.created_at.strftime("%m/%d/%y, %k:%M"))
          expect(page).to have_content(post.comments.count)
          expect(page).to have_content(post.title)
      end

  end

end
