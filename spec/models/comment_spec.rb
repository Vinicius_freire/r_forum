require 'rails_helper'

class Comment
  include Mongoid::Document
end

RSpec.describe Comment, type: :model do
  it { is_expected.to be_mongoid_document }
end
