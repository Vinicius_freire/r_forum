require 'rails_helper'

RSpec.describe Post, type: :model do
  it { is_expected.to be_mongoid_document }

  before(:each) do
    @post = FactoryBot.create(:post)
  end

  describe 'Validations' do
    context 'title' do
      it 'should not be blank' do
          post_repeated = FactoryBot.create(:post)
          expect(post_repeated).to be_valid
      end
    end
  end 
  
end
