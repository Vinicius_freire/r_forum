require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to be_mongoid_document }

  before(:each) do
    @user = FactoryBot.create(:user)
  end

  describe 'Associations' do
    it { is_expected.to have_fields(:email, :name, :nickname) }
  end
  
  describe 'Validations' do
    context 'email' do
        it 'should be unique' do
            user_repeated = FactoryBot.create(:user)
            user_repeated.email = @user.email
            expect(user_repeated).to_not be_valid
        end

        it 'should not be blank' do
            user_repeated = FactoryBot.create(:user)
            user_repeated.email = nil
            expect(user_repeated).to_not be_valid
        end
    end

    context 'name' do
        it 'should not be blank' do
            @user.name = nil
            expect(@user).to_not be_valid
        end
    end

    context 'nickname' do
      it 'should be unique' do
        user_repeated = FactoryBot.create(:user)
        user_repeated.nickname = @user.nickname
        expect(user_repeated).to_not be_valid
      end

      it 'should not be blank' do
          @user.nickname = nil
          expect(@user).to_not be_valid
      end
    end
    
  end 
end
