FactoryBot.define do
  factory :post do
      title { FFaker::Lorem.sentence }
      user { FactoryBot.create(:user) }
  end
end
