FactoryBot.define do
  factory :comment do
      comment { FFaker::Lorem.sentence }
      user { FactoryBot.create(:user) }
      commentable { FactoryBot.create(:post) }
  end
end
