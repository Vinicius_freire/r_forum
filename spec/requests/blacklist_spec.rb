require 'rails_helper'
 
RSpec.describe "Blacklist", type: :request do

  before do
    ["gun", "violence", "killer"].each do |word|
      Blacklist.create(word: word)
    end

    @user = create(:user)
    sign_in @user
  end

  describe "Create a new post" do
    before do
      post "/posts", params: {post: {title: "Vi0lence is a bad thing because always someone use gun...."}}
    end

    it "returns found" do
        expect(response.status).to eql(302)
    end

    it "returns current formatted post" do
        last_post = Post.all.last
        formatted_comment = "***** is a bad thing because always someone use *****...."
        expect(last_post.title).to eql(formatted_comment) 
    end
  end

  describe "Create a new comment for the last post" do
    before do
      @post = create(:post)
      params = {comment: "Violence is a bad thing because always someone use gun...."}
      begin
        post "/comments", params: {comment: params, post_id: @post.id} 
      rescue
      end
    end

    it "returns current comment and formatted" do
        last_post = Post.all.last
        new_comment = Comment.all.last
        expect(@post).to eql(last_post)
        expect(@post.comments.last.id).to eql(new_comment.id)
        formatted_comment = "***** is a bad thing because always someone use *****...."
        expect(new_comment.comment).to eql(formatted_comment) 
    end
  end
 
end
