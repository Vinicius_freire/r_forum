require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  describe "GET #show" do
    before(:each) do
      1.times do
        FactoryBot.create(:comment)
      end
    end

    context "See a created comment" do
      before(:each) do
        @user = create(:user)
        sign_in user
        get :show, params: {}
      end
    end
    
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "Post #create" do
    context "create a new post" do
      before(:each) do
        @user = create(:user)
        sign_in user
        post :create, params: {comment: {comment: attributes_for(:comment)}}
      end
    end
    
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end
  
end
