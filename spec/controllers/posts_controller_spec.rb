require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  describe "GET #index" do
    before(:each) do
      10.times do
        FactoryBot.create(:post)
      end
    end

    context "See all posts created" do
      before(:each) do
        @user = create(:user)
        sign_in user
        get :index
      end
    end
    
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "created all posts" do
      posts = Post.all
      expect(posts.count).to eq(10)
    end
  end

  describe "GET #show" do
    before(:each) do
      1.times do
        FactoryBot.create(:post)
      end
    end

    context "See a created" do
      before(:each) do
        @user = create(:user)
        sign_in user
        get :show
      end
    end
    
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "Post #create" do
    context "create a new post" do
      before(:each) do
        @user = create(:user)
        sign_in user
        @title = "new post"
        post :create, params: {post: {title: @title}}
      end
    end
    
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end

  describe "Delete #destroy" do
    context "See all posts created" do
      before(:each) do
        @user = create(:user)
        sign_in user
        @post = FactoryBot.create(:post)
        post :create, params: {id: @post.id}
      end
    end
    
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "there is not any posts" do
      expect(Post.all.count).to eq(0)
    end
  end

end
