* ## RFORUM

  #####  About

  Forum is a web page tool designed to promote discussion through posted messages addressing the same issue. It is also called a "community" or "board". It was created by John Smith in 1987. RForum is an attractive and beautiful website to promote discussion across the world. 

  

  #####  Operation

  - This app was deployed in cloud services DigitalOcean.

  - [Link to app ](<http://167.99.53.39:3000/>)

  - **Log in as admin**, user: vini@email.com and password: 12345678

    

  #####  System dependencies

  - Docker (<https://www.docker.com/>)
  - Docker-compose

  #####  

  ##### Technologies

  * Ruby 2.5
  * Rails 5.2
  * Mongodb
  
  
  
#####  Gems
  
- gem 'devise' multi-client and secure token-based authentication for Rails.
  
- gem 'mongoid' Mongoid is an ODM (Object-Document Mapper) framework for MongoDB in Ruby.
  
- gem 'kaminari-mongoid' Kaminari Mongoid adapter.
  
- gem 'raiils-admin' simple, RailsAdmin is a Rails engine that provides an easy-to-use interface for managing your data.
  
- gem 'pundit' to build a simple, robust and scaleable authorization system
  
- gem 'brakeman' brakeman is a security scanner for Ruby on Rails applications.
  
  - The brakeman results is in docs path as [brakeman-report.html]() 
  
- gem 'active_model_serializers' for object serialization
  
- The following gems were used to help in the automated tests
  
  - rspec
  
  - capybara
  
  - ffaker
  
  - factory_bot_rails
  
  - The Rspec results is in docs path /docs
  
    
  
  #####  Step by step to run the project in localhost using docker-compose
  
  1. Clone project from git
  
     ```
   git clone https://Vinicius_freire@bitbucket.org/Vinicius_freire/r_forum.git
     ```
     
  2.  Open **local-dev** branch **Important**

     ```
   git checkout local-dev
     ```
  2. Build the containers and install all dependencies
  
   ```
     docker-compose run --rm app bundle install
   ```
  
  3. Now, create database, if needed:
  
   ```
     docker-compose run --rm app bundle exec rails db:create
   ```
  
4. Start the server with:
  
     ```
     docker-compose up
     ```
  
5. Open browser in Localhost:3000. **The first registed user will be the admin.**
  6. **How to create a new word in blacklist:**
     1. Login with admin user
     2. Click Admin on TopBar
     3. Click Blacklist in Dashboard
     4. Create a new blacklist

**Project's goal**

1. Pagination: It was developed in PostController inside the method index. 
  2. Tree comments: It was developed in CommentController inside the method show. (Serializer)  
3. Blacklist: It was developed in services/blacklist

  

  ###  Links

  #####  [My Git](https://gitlab.com/vini.freire.oliveira) 

  #####  [My LikedIn](https://www.linkedin.com/in/vinicius-freire-b53507107/)
